package de.elinja.sams.backend.cmdline.entrypoint;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import de.elinja.sams.backend.api.commandline.SamsCommandLineCommand;

@RunWith(MockitoJUnitRunner.class)
public class CommandLineEntryPointTest {

    private void runWithBundle0(final Bundle bundle, final CommandLineEntryPoint entryPoint)
            throws InterruptedException {
        final BundleContext context = Mockito.mock(BundleContext.class);
        when(context.getBundle(0)).thenReturn(bundle);
        entryPoint.activate(context);
        Thread.sleep(500); // wait some time to get the other thread a change
                           // to
                           // do its works
    }

    private void setCmdlineArgs(final CommandLineEntryPoint entryPoint, final String[] cmdlineArgs) {
        final Map<String, Object> launcherProperties = new HashMap<>();
        launcherProperties.put("launcher.arguments", cmdlineArgs);
        entryPoint.setCmdlineArgs(null, launcherProperties);
    }

    private SamsCommandLineCommand addCommand(final CommandLineEntryPoint entryPoint, final String commandName) {
        final SamsCommandLineCommand command = mock(SamsCommandLineCommand.class);
        final Map<String, Object> commandProperties = new HashMap<>();
        commandProperties.put("sams.cmdline.command", commandName);
        entryPoint.addCommand(command, commandProperties);
        return command;
    }

    @Test
    public void testRunExistingCommand() throws Exception {

        final CommandLineEntryPoint entryPoint = new CommandLineEntryPoint();
        final SamsCommandLineCommand command = addCommand(entryPoint, "foo");
        final String[] cmdlineArgs = new String[] { "foo", "bar", "grunt", "thud" };
        setCmdlineArgs(entryPoint, cmdlineArgs);

        final Bundle bundle = mock(Bundle.class);
        runWithBundle0(bundle, entryPoint);

        verify(command, times(1)).runCommand(new String[] { "bar", "grunt", "thud" });
        verify(command, never()).getUsage();
        verify(bundle, times(1)).stop();
    }

    @Test
    public void testRunNonExistingCommand() throws Exception {

        final CommandLineEntryPoint entryPoint = new CommandLineEntryPoint();
        final SamsCommandLineCommand command = addCommand(entryPoint, "foo");
        final String[] cmdlineArgs = new String[] { "nonexisting", "bar", "grunt", "thud" };
        setCmdlineArgs(entryPoint, cmdlineArgs);

        final Bundle bundle = mock(Bundle.class);
        runWithBundle0(bundle, entryPoint);

        verify(command, never()).runCommand(any(String[].class));
        verify(command, times(1)).getUsage();
        verify(bundle, times(1)).stop();
    }

    @Test
    public void testRunNonCommand() throws Exception {

        final CommandLineEntryPoint entryPoint = new CommandLineEntryPoint();
        final SamsCommandLineCommand command = addCommand(entryPoint, "foo");
        final String[] cmdlineArgs = new String[] {};
        setCmdlineArgs(entryPoint, cmdlineArgs);

        final Bundle bundle = mock(Bundle.class);
        runWithBundle0(bundle, entryPoint);

        verify(command, never()).runCommand(any(String[].class));
        verify(command, times(1)).getUsage();
        verify(bundle, times(1)).stop();
    }

    @Test
    public void testRemoveCommand() throws Exception {

        final CommandLineEntryPoint entryPoint = new CommandLineEntryPoint();
        final SamsCommandLineCommand command = addCommand(entryPoint, "foo");

        // now remove the command
        final Map<String, Object> commandProperties = new HashMap<>();
        commandProperties.put("sams.cmdline.command", "foo");
        entryPoint.removeCommand(command, commandProperties);

        final String[] cmdlineArgs = new String[] { "foo", "bar", "grunt", "thud" };
        setCmdlineArgs(entryPoint, cmdlineArgs);

        // should not run foo since it has been removed
        final Bundle bundle = mock(Bundle.class);
        runWithBundle0(bundle, entryPoint);

        verify(command, never()).runCommand(any(String[].class));
        verify(command, never()).getUsage();
        verify(bundle, times(1)).stop();

    }

}
