package de.elinja.sams.backend.cmdline.entrypoint;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import de.elinja.sams.backend.api.commandline.SamsCommandLineCommand;

/**
 * The command line entry point.
 *
 * Holds references to all {@link SamsCommandLineCommand} commands and calls the
 * that is specified via the first command line parameter.
 *
 * @author joerg
 *
 */
@Component(immediate = true, enabled = true)
public class CommandLineEntryPoint {

    private static final String CMDLINE_COMMAND_SERVICE_PROPERTY = "sams.cmdline.command";
    private final Map<String, SamsCommandLineCommand> commands;
    private String[] cmdlineArgs;

    public CommandLineEntryPoint() {
        commands = Collections.synchronizedMap(new HashMap<>());
    }

    @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE, target = "(sams.cmdline.command=*)")
    protected void addCommand(final SamsCommandLineCommand command, final Map<String, Object> commandProperties) {
        commands.put(getCommandNameFromProperties(commandProperties), command);
    }

    protected void removeCommand(final SamsCommandLineCommand command, final Map<String, Object> commandProperties) {
        commands.remove(getCommandNameFromProperties(commandProperties), command);
    }

    private String getCommandNameFromProperties(final Map<String, Object> commandProperties) {
        return commandProperties.get(CMDLINE_COMMAND_SERVICE_PROPERTY).toString();
    }

    /**
     * Gets the command line arguments from the aQuote launcher.
     *
     * @param object
     *            the aQuote launcher (neigther type nor object of interest, but
     *            needed to fullfill interface requirenment)
     * @param launcherProperties
     *            the launcher properties
     */
    @Reference(target = "(launcher.arguments=*)")
    protected void setCmdlineArgs(final Object object, final Map<String, Object> launcherProperties) { // NOSONAR
        final Object args = launcherProperties.get("launcher.arguments");
        if (args instanceof String[]) {
            cmdlineArgs = (String[]) args;
        }
    }

    @Activate
    protected void activate(final BundleContext context) {
        printOut("Welcome to the sams commandline tool.");
        printOut();
        runCommandInOwnThread(context);
    }

    /**
     * Starts a new thread that runs the command specified on the commandline
     * and stops the OSGi framework afterwards.
     *
     * @param context
     */
    private void runCommandInOwnThread(final BundleContext context) {
        final Thread t = new Thread(() -> {
            runCommandSpecifiedOnCmdline();
            shutdownOsgiFramework(context);
        });

        t.start();
    }

    private void runCommandSpecifiedOnCmdline() {

        if (cmdlineArgs.length == 0) {
            printOut("Please specify a command");
            printUsage();
            return;
        }

        final String commandName = cmdlineArgs[0];
        final SamsCommandLineCommand commandImpl = commands.get(commandName);

        if (commandImpl != null) {
            final String[] internalArguments = getTailOfArray(cmdlineArgs);
            commandImpl.runCommand(internalArguments);
        } else {
            printOut("Unknown command: " + commandName);
            printUsage();
        }
    }

    private static String[] getTailOfArray(final String[] arrayOfStrings) {
        return Arrays.copyOfRange(arrayOfStrings, 1, arrayOfStrings.length);
    }

    private void shutdownOsgiFramework(final BundleContext context) {
        try {
            context.getBundle(0).stop();
        } catch (final BundleException e) {
            printOut(e);
        }
    }

    @Deactivate
    protected void deactivate() {
        printOut("Sams command line tool terminates.");
    }

    private static void printOut() {
        printOut("");
    }

    private static void printOut(final Throwable e) {
        e.printStackTrace(); // NOSONAR
    }

    private static void printOut(final String stringToPrint) {
        System.out.println(stringToPrint); // NOSONAR
    }

    private void printUsage() {
        printOut();
        printOut("Usage: ");
        printOut();

        commands.entrySet().forEach(commandEntry -> printCommandUsage(commandEntry.getKey(), commandEntry.getValue()));
    }

    private void printCommandUsage(final String commandName, final SamsCommandLineCommand commandImpl) {
        printOut(commandName + ":");

        commandImpl.getUsage()//
                .forEach(usageLine -> printOut("\t" + usageLine));
        printOut();
    }

}
