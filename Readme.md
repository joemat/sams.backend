*S*tore *A*ll *M*y *S*taff
==========================

A simple application to store all kind of documents.

Current status: early development

## Build status

* ![Build status](https://codeship.com/projects/9a7fe940-986f-0133-e993-3e3b216d16ca/status?branch=master)
* [![Build Status](https://drone.io/bitbucket.org/joemat/sams.backend/status.png)](https://drone.io/bitbucket.org/joemat/sams.backend/latest)
* [![Build Status](https://semaphoreci.com/api/v1/projects/51732b37-2fad-4c46-9bee-51d6c4dc1c5c/655193/badge.svg)](https://semaphoreci.com/joemat/sams-backend)


## Build

```
./gradlew clean build export
```

## Build with findbugs checks and code coverage report

```
./gradlew findbugsMain jacocoTestReport

## Run

```
 java -jar sams.backend.importer.impl/generated/distributions/executable/samsCmd.jar

```
