package de.elinja.sams.backend.api.commandline;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * The interface to be implemented by sams command line tools.
 *
 * Implementing services must also have the {@code sams.cmdline.command} service
 * property set. (This specifies the name of the command to be passed as first
 * parameter.)
 *
 * @see sams.backend.cmdline.entrypoint for the implementation of the entry
 *      point
 * @see ConsumerType
 * @since 1.0
 */
@ConsumerType
public interface SamsCommandLineCommand {

    void runCommand(String[] arguments);

    List<String> getUsage();
}
