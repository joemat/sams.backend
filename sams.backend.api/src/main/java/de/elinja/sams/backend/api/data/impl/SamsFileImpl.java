package de.elinja.sams.backend.api.data.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.elinja.sams.backend.api.data.SamsFile;
import de.elinja.sams.backend.api.data.SamsFileFeature;

public class SamsFileImpl implements SamsFile {

    private final String id;
    private final Map<String, SamsFileFeatureImpl> features = new HashMap<>();

    private final Map<String, String> properties = new HashMap<>();

    public SamsFileImpl(final String id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#getId()
     */
    @Override
    public String getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#getProperty(java.lang.
     * String)
     */
    @Override
    public String getProperty(final String propertyName) {
        return properties.get(propertyName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#putPropery(java.lang.
     * String, java.lang.String)
     */
    @Override
    public String putProperty(final String propertyName, final String propertyValue) {
        return properties.put(propertyName, propertyValue);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#putAll(java.util.Map)
     */
    @Override
    public void putAllProperties(final Map<String, String> properties) {
        this.properties.putAll(properties);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#getFeature(java.lang.
     * String)
     */
    @Override
    public SamsFileFeature getFeature(final String featureName) {
        return features.get(featureName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.elinja.sams.backend.api.data.impl.SamsFile#removeFeature(java.lang.
     * String)
     */
    @Override
    public SamsFileFeature removeFeature(final String featureName) {
        return features.remove(featureName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.elinja.sams.backend.api.data.impl.SamsFile#createFeature(java.lang.
     * String)
     */
    @Override
    public SamsFileFeature createFeature(final String featureName) {
        final SamsFileFeatureImpl newFeature = new SamsFileFeatureImpl();
        features.put(featureName, newFeature);
        return newFeature;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#listFeatures()
     */
    @Override
    public Set<String> listFeatures() {
        return features.keySet();
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.elinja.sams.backend.api.data.impl.SamsFile#listPropertioes()
     */
    @Override
    public Set<String> listProperties() {
        return properties.keySet();
    }

    @Override
    public String toString() {
        return super.toString() + "(id=" + id + ")";
    }

}
