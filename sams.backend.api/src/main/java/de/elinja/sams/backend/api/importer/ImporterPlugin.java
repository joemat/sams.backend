package de.elinja.sams.backend.api.importer;

import java.nio.file.Path;

import org.osgi.annotation.versioning.ConsumerType;

import de.elinja.sams.backend.api.data.SamsFile;

/**
 * The interface to be implemented by sams importer plugins. Implementing
 * service need also to have the service property {@code sams.importer.name} set
 * to specify the name of the importer plugin.
 * 
 * @see sams.backend.importer.impl for the consumer of this interface
 * @see ConsumerType
 * @since 1.0
 */
@ConsumerType
@FunctionalInterface
public interface ImporterPlugin {

    /**
     * Processes the passed file and may enhance {@code samsFile} with
     * additional information.
     */
    SamsFile addInformationForFile(Path file, SamsFile samsFile);

}
