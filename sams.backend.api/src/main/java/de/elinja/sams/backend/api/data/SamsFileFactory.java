package de.elinja.sams.backend.api.data;

import de.elinja.sams.backend.api.data.impl.SamsFileImpl;

public final class SamsFileFactory {

    private SamsFileFactory() {
        // disabled
    }

    public static SamsFile createSamsFile(final String id) {
        return new SamsFileImpl(id);
    }

}
