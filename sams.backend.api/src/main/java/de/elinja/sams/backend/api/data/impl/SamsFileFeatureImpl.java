package de.elinja.sams.backend.api.data.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.elinja.sams.backend.api.data.SamsFileFeature;

public class SamsFileFeatureImpl implements SamsFileFeature {

    private final Map<String, String> properties = new HashMap<>();

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.elinja.sams.backend.api.data.impl.SamsFileFeature#getProperty(java.
     * lang.String)
     */
    @Override
    public String getProperty(final String propertyName) {
        return properties.get(propertyName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.elinja.sams.backend.api.data.impl.SamsFileFeature#putPropery(java.lang
     * .String, java.lang.String)
     */
    @Override
    public String putProperty(final String propertyName, final String propertyValue) {
        return properties.put(propertyName, propertyValue);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.elinja.sams.backend.api.data.impl.SamsFileFeature#putAll(java.util.
     * Map)
     */
    @Override
    public void putAllProperties(final Map<String, String> properties) {
        this.properties.putAll(properties);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.elinja.sams.backend.api.data.impl.SamsFileFeature#listPropertioes()
     */
    @Override
    public Set<String> listProperties() {
        return properties.keySet();
    }

}
