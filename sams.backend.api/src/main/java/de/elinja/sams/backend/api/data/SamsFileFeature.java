package de.elinja.sams.backend.api.data;

import java.util.Map;
import java.util.Set;

public interface SamsFileFeature {

    String getProperty(String propertyName);

    String putProperty(String propertyName, String propertyValue);

    void putAllProperties(Map<String, String> properties);

    Set<String> listProperties();

}