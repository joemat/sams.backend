package de.elinja.sams.backend.api.data;

import java.util.Map;
import java.util.Set;

/**
 * A sams file structure.
 *
 * @author joerg
 *
 */
public interface SamsFile {

    String getId();

    String getProperty(String propertyName);

    String putProperty(String propertyName, String propertyValue);

    void putAllProperties(Map<String, String> properties);

    SamsFileFeature getFeature(String featureName);

    SamsFileFeature removeFeature(String featureName);

    SamsFileFeature createFeature(String featureName);

    Set<String> listFeatures();

    Set<String> listProperties();

}