package sams.backend.api.data;

import static org.junit.Assert.*;

import org.junit.Test;

import de.elinja.sams.backend.api.data.SamsFile;
import de.elinja.sams.backend.api.data.SamsFileFactory;

public class SamsFileFactoryTest {

	@Test
	public void testCreateSamsFile() {
		SamsFile samsFile = SamsFileFactory.createSamsFile("foo");
		assertEquals("foo", samsFile.getId());
	}

}
