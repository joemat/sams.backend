package de.elinja.sams.backend.api.data.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import de.elinja.sams.backend.api.data.SamsFileFeature;

public class SamsFileFeatureImplTest {

	@Test
	public void testPutAndGetProperty() {
		SamsFileFeature samsFileFeature = new SamsFileFeatureImpl();
		samsFileFeature.putProperty("batz", "grunt");
		assertEquals("grunt", samsFileFeature.getProperty("batz"));
	}

	@Test
	public void testPutAllProperties() {
		Map<String, String> props = new HashMap<>();
		props.put("eins", "1");
		props.put("zwei", "2");
		SamsFileFeature samsFileFeature = new SamsFileFeatureImpl();
		samsFileFeature.putAllProperties(props);
		assertEquals("1", samsFileFeature.getProperty("eins"));
		assertEquals("2", samsFileFeature.getProperty("zwei"));
	}

	@Test
	public void testListProperties() {
		Map<String, String> props = new HashMap<>();
		props.put("eins", "1");
		props.put("zwei", "2");
		
		SamsFileFeature samsFileFeature = new SamsFileFeatureImpl();
		samsFileFeature.putAllProperties(props);
		Set<String> listed = samsFileFeature.listProperties();
		
		assertTrue(listed.contains("eins"));
		assertTrue(listed.contains("zwei"));		
	}

}
