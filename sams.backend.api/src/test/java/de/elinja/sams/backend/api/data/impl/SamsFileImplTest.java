package de.elinja.sams.backend.api.data.impl;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import de.elinja.sams.backend.api.data.SamsFile;
import de.elinja.sams.backend.api.data.SamsFileFeature;

public class SamsFileImplTest {

	@Test
	public void testGetId() {
		SamsFile samsFile = new SamsFileImpl("bar");
		assertEquals("bar", samsFile.getId());
	}

	@Test
	public void testPutAndGetProperty() {
		SamsFile samsFile = new SamsFileImpl("bar");
		samsFile.putProperty("batz", "grunt");
		assertEquals("grunt", samsFile.getProperty("batz"));
	}

	@Test
	public void testPutAllProperties() {
		Map<String, String> props = new HashMap<>();
		props.put("eins", "1");
		props.put("zwei", "2");
		SamsFile samsFile = new SamsFileImpl("bar");
		samsFile.putAllProperties(props);
		assertEquals("1", samsFile.getProperty("eins"));
		assertEquals("2", samsFile.getProperty("zwei"));
	}

	@Test
	public void testListPropertioes() {
		Map<String, String> props = new HashMap<>();
		props.put("eins", "1");
		props.put("zwei", "2");
		
		SamsFile samsFile = new SamsFileImpl("bar");
		samsFile.putAllProperties(props);
		Set<String> listed = samsFile.listProperties();
		
		assertTrue(listed.contains("eins"));
		assertTrue(listed.contains("zwei"));		
	}

	@Test
	public void testGetFeature() {
		SamsFile samsFile = new SamsFileImpl("bar");
		SamsFileFeature f1 = samsFile.createFeature("featureOne");
		assertEquals(f1, samsFile.getFeature("featureOne"));
	}

	@Test
	public void testRemoveFeature() {
		SamsFile samsFile = new SamsFileImpl("bar");
		SamsFileFeature f1 = samsFile.createFeature("featureOne");
		assertEquals(f1, samsFile.removeFeature("featureOne"));
		assertNull(samsFile.removeFeature("featureOne"));
	}

	@Test
	public void testCreateFeature() {
		SamsFile samsFile = new SamsFileImpl("bar");
		assertNotNull(samsFile.createFeature("featureOne"));
	}

	@Test
	public void testListFeatures() {
		SamsFile samsFile = new SamsFileImpl("bar");
		samsFile.createFeature("featureOne");
		samsFile.createFeature("featureTwo");
		
		Set<String> listed = samsFile.listFeatures();
		assertTrue(listed.contains("featureOne"));
		assertTrue(listed.contains("featureTwo"));

	}

}
