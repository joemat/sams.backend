package de.elinja.sams.backend.importer.impl;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import de.elinja.sams.backend.api.commandline.SamsCommandLineCommand;
import de.elinja.sams.backend.api.data.SamsFile;
import de.elinja.sams.backend.api.data.SamsFileFactory;
import de.elinja.sams.backend.api.importer.ImporterPlugin;

@Component(property = {"sams.cmdline.command:String=import"})
public class SamsImporter implements SamsCommandLineCommand {

	private final class ImporterFileVisitor implements FileVisitor<Path> {
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			
			String fileUri = file.toUri().toString();
			SamsFile samsFile = SamsFileFactory.createSamsFile(fileUri);
			samsFile.putProperty("path", file.toUri().toString());

			plugins.forEach((name, plugin) -> {
				System.out.println("Calling plugin " + name);
				plugin.addInformationForFile(file, samsFile);
			});

			System.out.println("---- " + file.toString());
			System.out.println("FileInfo: " + samsFile.toString());
			System.out.println();

			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}
	}

	final private Map<String, ImporterPlugin> plugins;

	public SamsImporter() {
		plugins = Collections.synchronizedMap(new HashMap<>());
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE, target="(sams.importer.name=*)")
	protected void addPlugin(ImporterPlugin plugin, Map<String, Object> pluginProperties) {
		plugins.put(pluginProperties.get("sams.importer.name").toString(), plugin);
	}

	protected void removePlugin(ImporterPlugin plugin, Map<String, Object> pluginProperties) {
		plugins.remove(pluginProperties.get("sams.importer.name").toString(), plugin);
	}


	public void runCommand(String[] cmdlineArgs) {
		importFiles(cmdlineArgs);
	}
	
	public void importFiles(String[] cmdlineArgs) {

		Set<FileVisitOption> options = new TreeSet<>();
		options.add(FileVisitOption.FOLLOW_LINKS);

		FileVisitor<? super Path> visitor = new ImporterFileVisitor();

		String path = ".";

		if ((cmdlineArgs != null) && (cmdlineArgs.length > 0)) {
			path = cmdlineArgs[0];
			System.out.println("Using path:" + path);
		}

		Path dir = FileSystems.getDefault().getPath(path);
		try {
			Files.walkFileTree(dir, options, Integer.MAX_VALUE, visitor);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<String> getUsage() {
		String[] usage = new String[] {
				"import [dir]",
				"\t[dir] directory to start with",
				"",
				"Imports documents from a specified directory."
		};
		return Arrays.asList(usage);
	}
}
